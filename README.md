# Sweet Nothings - The Powerful Whisper and Chat Module

This module leverages Foundry v9's new Keybinding API to register a simple, configurable keybinding that brings up a simple yet powerful dialog option to make whispering and chat messages flexible!

## Usage

By default, the keybinding is ALT-W.  Pressing this will present the Whisper Sweet Nothings dialog box.  There are two modes - Whisper, and Chat.  Whisper is the default, and you can check as many recipients as you want.

As a nice shortcut, you may use ALT-R to reply to the last whisper you have received.  This will automatically select the person who sent the whisper.

Alternatively, you can swap to the Chat mode, which also allows you to set the various chat modes allowed by Foundry.  In Character (IC) and Emote chat modes will also generate the chat bubble above your token's head.  If you are the GM, this will also set the speaker to your selected token.

## Configuration

Sweet Nothings main configuration is through Foundry's Configure Controls dialog, under the Settings Sidebar.  Sweet Nothings registers it's own category, and you can modify the keybinding shortcut here.

You can also define the defaults for the dialog, either through the normal settings sidebar or using the cog at the top of the Whisper Sweet Nothings dialog.

![The Whisper Dialog](https://gitlab.com/geekswordsman/SweetNothings/-/wikis/uploads/09d590a32ce2123d7c83c1dfbabc4c76/Sweet_Nothings_Whisper_Dialog.JPG)

![The Chat Dialog](https://gitlab.com/geekswordsman/SweetNothings/-/wikis/uploads/a9d2f527bf42d62f561cff27c1ddfdfe/Sweet_Nothings_Chat_Dialog.JPG)

![Configuration Dialog](https://gitlab.com/geekswordsman/SweetNothings/-/wikis/uploads/2a970fe1a0988e8eb77d8881b66746dd/Sweet_Nothings_Options.JPG)

![Sample Output](https://gitlab.com/geekswordsman/SweetNothings/-/wikis/uploads/f2acfee538cdbd00a6870f55910dcdf6/Sweet_Nothings_Sample.JPG)